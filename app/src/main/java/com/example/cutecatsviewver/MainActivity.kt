package com.example.cutecatsviewver

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.koushikdutta.ion.Ion
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedInputStream
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showCuteCat(view: View) {

        YoYo.with(Techniques.Wobble)
            .duration(3000)
            .playOn(show_button)

        Ion.with(this)
            .load("https://api.thecatapi.com/v1/images/search?limit=6")
            .asString()
            .setCallback{ _, result ->
                loadCatImage(result)
            }
    }


    private fun loadCatImage(array: String) {
        val imagesArray = JSONArray(array)
        cats_grid.removeAllViews()
        for (i in 0 .. imagesArray.length() - 1) {
            val url = imagesArray.getJSONObject(i).getString("url")
            val img = ImageView(this)
            cats_grid.addView(img)
            Picasso.get()
                .load(url)
                .into(img)
            val params = img.layoutParams as ViewGroup.LayoutParams
            params.width = resources.displayMetrics.widthPixels/2
            params.height = resources.displayMetrics.widthPixels/3
        }
    }
}